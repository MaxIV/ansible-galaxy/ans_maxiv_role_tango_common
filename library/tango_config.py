#!/usr/bin/python

# Copyright: (c) 2019, KITS <kits@maxiv.lu.se
# Written by Vasileios Martos <vasmar at maxiv.lu.se>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    "metadata_version": "1.1",
    "status": ["preview"],
    "supported_by": "kits_controls@maxiv.lu.se",
}

DOCUMENTATION = """
---
module: tango_config

short_description: TangoDB Configuration

version_added: "0.1"

description:
    - "Using Ansible in order to configure idempotently a specified TangoDB using dsconfig lib"

options:
    src:
        description:
            - Dict structure following the Tango Hierarchy
        type: str
        required: false
    tango_server:
        description:
            - Tango Server parameter, in case is not defined
              it will use the same value with Device Class
        type: str
        required: false
    server_instance:
        description:
            - Server instance parameter, required when it
              is defined with device_class & device_instance
        type: str
        required: false
    device_class:
        description:
            - DeviceClass parameter, required when it
              is defined with server_instance & device_instance.
        type: str
        required: false
    device_instance:
        description:
            - Device instance parameter, required when it
              is defined with server_instance & device_class.
        type: str
        required: false
    device_properties:
        description:
            - Device properties parameter, required when it is defined
              with server_instance, device_class & device_instance.
        type: dict
        required: false
    attribute_properties:
        description:
            - Device attribute properties parameter, required when it is defined
              with server_instance, device_class & device_instance.
        type: dict
        required: false

    devices:
        description:
            - List of devices in order to configure in bulk the tango system with one request.
              The format reuse the arguments tango_server, server_instance, device_class, device_instance, device_properties, attribute_properties.
              This is a way to workaround the deprecation of the squash actions
        type: list of dict
        required: false

    tangodb:
        description:
            - specify TangoDB host, by default using the $TANGO_HOST
        required: false
    update_only:
        description:
            - by default it is True, setting that argument to False
              we will remove entries which are not in the current config

requirements:
- python-dsconfig >= 1.2.4
author:
    - KITS kits_controls@maxiv.lu.se
"""

EXAMPLES = """
  - name: Import Tango Data
    tango_config:
      server_instance: test
      device_class: TangoTest
      device_instance: sys/tg_test/1
      device_properties:
        Test:
          - "Hello World!!"
      attribute_properties:
        short_scalar:
          unit:
            - 'mV'

  - name: Import Tango Data
    tango_config:
      tango_server: TangoTest
      server_instance: "TestInstance_{{ item[0] }}"
      device_class: TestClass
      device_instance: "test{{ item[0] }}/device/{{ item[1] }}"
      device_properties:
        Test:
          - "Hello World!!"
    with_nested:
      - [1, 2]
      - [1, 2, 3]

  - name: Configure in bulk
    tango_config:
      devices:
        - tango_server: TangoTest
          server_instance: TestInstance_1
          device_class: TestClass
          device_instance: "test/device/1"
          device_properties:
            Test:
              - "Hello World!!"
        - tango_server: TangoTest
          server_instance: TestInstance_1
          device_class: TestClass
          device_instance: "test/device/2"
          device_properties:
            Foo:
              - "Bar"
"""

RETURN = """
original_message:
    description: The original name param that was passed in
    type: str
message:
    description: The output message that the sample module generates
"""


import json
from ansible.module_utils.basic import AnsibleModule
from collections import defaultdict
import tango

try:
    # Dsconfig calls
    from dsconfig.formatting import (
        normalize_config,
        clean_metadata,
    )
    from dsconfig.dump import get_db_data
    from dsconfig.configure import configure

    HAS_DSCONFIG = True
except ImportError:
    HAS_DSCONFIG = False


# UTILS
def append_tango_struct(
    servers,
    tango_server,
    server_instance,
    device_class,
    device_instance,
    device_properties,
    attribute_properties,
):
    """
    Create a dict data structure following the TANGODB Schema
    """
    # Register the device
    device = servers[tango_server][server_instance][device_class][device_instance]

    # The associated device properties
    if device_properties:
        device["properties"] = device_properties

    # The associated attributes properties
    if attribute_properties:
        device["attribute_properties"] = attribute_properties


def parse_bulk_configuration(servers, params):
    """
    Parse a dictionary containing the arguments to make the tango dsconfig
    """
    for device in params:
        if "server" in device:
            server = device["server"]
        else:
            server = device["class"]

        # Properties are not mandatory
        if "properties" in device:
            for i in device["properties"]:
                if not isinstance(device["properties"][i], list):
                    device["properties"][i] = [str(device["properties"][i])]
                else:
                    device["properties"][i] = [str(prop) for prop in device["properties"][i]]
            properties = device["properties"]
        else:
            properties = None

        # Attributes Properties are not mandatory
        if "attributes_properties" in device:
            attr_properties = device["attributes_properties"]
        else:
            attr_properties = None

        append_tango_struct(
            servers,
            server,
            device["instance"],
            device["class"],
            device["device"],
            properties,
            attr_properties,
        )


def parse_configuration(servers, params):
    """
    Parse a dictionary containing the arguments to make the tango dsconfig
    """
    tango_servers = params["tango_server"]

    server_instances = params["server_instance"]
    device_classes = params["device_class"]

    # Use device_class as tango_server if latter is not provided
    if not params["tango_server"]:
        tango_servers = device_classes
    else:
        tango_servers = params["tango_server"]

    device_instances = params["device_instance"]
    if params["device_properties"]:
        for i in params["device_properties"]:
            if not isinstance(params["device_properties"][i], list):
                params["device_properties"][i] = [str(params["device_properties"][i])]
            else:
                params["device_properties"][i] = [str(prop) for prop in params["device_properties"][i]]
    device_properties = params["device_properties"]
    attribute_properties = params["attribute_properties"]

    append_tango_struct(
        servers,
        tango_servers,
        server_instances,
        device_classes,
        device_instances,
        device_properties,
        attribute_properties,
    )


def parse_tango_arguments(module):
    """
    Get Ansible arguments and returns a dict(dict) structure
    """
    # Tango Structure is a dict of dict
    tango_structure = lambda: defaultdict(tango_structure)  # noqa E731
    servers = tango_structure()

    # Check if a bulk configuration is requested
    if module.params["devices"]:
        parse_bulk_configuration(servers["servers"], module.params["devices"])
    # Otherwise we take the one shot arguments
    else:
        parse_configuration(servers["servers"], module.params)

    return servers


# Module


def run_module():

    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        src=dict(type="str", required=False),
        tangodb=dict(type="str", required=False),
        update_only=dict(required=False, type="bool", default=True),
        tango_server=dict(required=False, type="str"),
        server_instance=dict(required=False, type="str"),
        device_class=dict(required=False, type="str"),
        device_instance=dict(required=False, type="str"),
        device_properties=dict(required=False, type="dict"),
        attribute_properties=dict(required=False, type="dict"),
        devices=dict(required=False, type="list"),
        dbfile=dict(required=False, type="path"),
    )
    # Extra parameter options
    required_one_of = [
        [
            "src",
            "server_instance",
            "device_class",
            "device_instance",
            "devices",
        ]
    ]

    mutually_exclusive = [
        ["src", "tango_server"],
        ["src", "server_instance"],
        ["src", "device_class"],
        ["src", "device_instance"],
        ["src", "device_properties"],
        ["src", "attribute_properties"],
        ["src", "devices"],
        ["devices", "tango_server"],
        ["devices", "server_instance"],
        ["devices", "device_class"],
        ["devices", "device_instance"],
        ["devices", "device_properties"],
        ["devices", "attribute_properties"],
    ]

    required_together = [["server_instance", "device_class", "device_instance"]]

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(changed=False, original_message="", message="")

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        required_together=required_together,
        required_one_of=required_one_of,
        mutually_exclusive=mutually_exclusive,
        supports_check_mode=True,
    )

    # Check Dependancies
    if not HAS_DSCONFIG:
        module.fail_json(
            msg="The 'dsconfig' python module (version 1.2.4 or newer) is required for the redis fact cache, 'pip/yum install python-dsconfig'"
        )

    # tangodb
    tangodb = module.params["tangodb"]
    try:
        if tangodb:
            db_host, db_port = tangodb.split(":")
            db = tango.Database(db_host, int(db_port))
        else:
            # get the dB object
            db = tango.Database()
    except Exception:
        module.fail_json(
            msg="Cant connect to the Tango DB, specify the correct DB using the 'tangodb' argument.'"
        )

    # cleanup_entries
    update_only = module.params["update_only"]

    # src
    # TODO if yaml then to json
    # or if src == None buld dict struct
    # with device/server/instance/properties and json it.
    if module.params["src"]:
        src = module.params["src"]
    else:
        src = json.dumps(parse_tango_arguments(module))

    # Format input src data
    data = json.loads(src)
    data = normalize_config(data)
    data = clean_metadata(data)

    # Init config procedure
    result["changed"] = False

    # Get DB Backup
    original = get_db_data(
        db,
        dservers=True,
        class_properties=True,
        attribute_properties=True,
        aliases=True,
    )
    # get the list of DB calls needed
    dbcalls = configure(
        data,
        original,
        update=update_only,
        ignore_case=True,
        strict_attr_props=False,
    )

    if dbcalls:
        result["changed"] = True

    # if is not Check.mode then write the changes to the db
    if not module.check_mode:

        for i, (method, args, kwargs) in enumerate(dbcalls):
            getattr(db, method)(*args, **kwargs)

    # Print Diff
    if module._diff:
        # Since it is a bit weird to print json diffs printing
        # the calls to the DB it can give a picture of the diff to the user.
        diff_report = [
            "{0}:{1}".format(method, args) for method, args, kwargs in dbcalls
        ]

        result.update({"diff": {"before": "", "after": "\n".join(diff_report)}})

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():

    run_module()


if __name__ == "__main__":
    main()
