#!/usr/bin/python

# Written by Vasileios Martos <vasmar at maxiv.lu.se>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    "metadata_version": "1.1",
    "status": ["preview"],
    "supported_by": "kits_controls@maxiv.lu.se",
}

DOCUMENTATION = """
---
module: tango_polling

short_description: Tango polling Configuration

version_added: "1.0"

description:
    - "Using Ansible in order to configure polling of Tango Devices"

options:
    polling_config:
        description:
            - Polling value you want to apply
        type: list
        elements: str
    tangodb:
        description:
            - specify TangoDB host, by default using the $TANGO_HOST
        required: false
        type: str

requirements:
- python-dsconfig >= 1.4.0
- python-pytango
author:
    - KITS vasmar@maxiv.lu.se
"""

EXAMPLES = """

vars:
    polling_config:
        - TangoTest:
            filters:
            device_pattern: sys/tg_test2/.*
            attributes:
            short_scalar_ro: 3004
            short_scalar: 3101

- name: Configure polling
  hosts: tangoconfig
  gather_facts: no
  tasks:
    - name: Import polling data to Tango
      tango_polling:
        polling_config: "{{ polling_config }}"

"""

RETURN = """
original_message:
    description: The original name param that was passed in
    type: str
message:
"""


from ansible.module_utils.basic import AnsibleModule
from collections import defaultdict
import re
import tango

# UTILS


def convert_poll_list(polling_list):
    """
    Gets a list and converts the elements
    of a dict N0:N1 pair of consecutive elements
    """
    it = iter(polling_list)
    res_dct = dict(zip(it, it))
    return res_dct


def get_class_devices(db, clss, filtering):
    """
    Returns a list of devices for a Class. Also
    can apply different filters i.e device or server
    """
    # Get the filters first
    server_pattern = filtering.get("server_pattern", "*")
    device_pattern = filtering.get("device_pattern", None)
    # Get devices from Class/Servers
    yaml_devices = db.get_device_name(server_pattern, clss)

    if device_pattern:
        # pattern = re.compile(device_pattern)
        yaml_devices = [
            dev
            for dev in yaml_devices
            if re.search(device_pattern, dev, flags=re.IGNORECASE)
        ]

    return yaml_devices


def compare_dicts(conf_dict, db_dict):
    """
    Gets 2 dicts of depth=1
    and extract the differences as
    list of tuples (node,subnode,old_value,new_value)
    """
    tuples_diffs = []
    for device, devicedata in conf_dict.items():
        for attr, new_polling in devicedata.items():
            # Get the polling from the node
            # of the db_dict
            old_value = db_dict.get(device, {}).get(attr, 0)
            if str(old_value) != str(new_polling):
                tuples_diffs.append((device, attr, old_value, new_polling))

    return tuples_diffs


def run_module():

    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        tangodb=dict(type="str", required=False),
        polling_config=dict(required=True, type="list"),
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(changed=False, original_message="", message="")

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True,
    )

    # Check Dependancies
    if tango.utils.Release.version_number < 900:
        module.fail_json(
            msg="The 'dsconfig' python module (version 1.4.0 or newer) is required 'pip/yum install python-dsconfig'"
        )

    # config data
    config_data = module.params["polling_config"]

    # tangodb
    tangodb = module.params["tangodb"]

    try:
        if tangodb:
            db_host, db_port = tangodb.split(":")
            db = tango.Database(db_host, int(db_port))
        else:
            # get the dB object
            db = tango.Database()
    except Exception:
        module.fail_json(
            msg="Cant connect to the Tango DB, specify the correct DB using the 'tangodb' argument.'"
        )

    # We need to create a cache dict {dev: { attr : polling }} for all
    # the device attributes we want to configure
    # which will prevent later excesive queries to TangoDB
    tangoDB_polling_cache = defaultdict()

    # Dict with the configuration
    # of the YAML file to d
    attr_cfg_list = defaultdict()

    for clss_data in config_data:

        # get class Name
        clss = next(iter(clss_data))

        # attributes to cfg
        yaml_attr_list = clss_data[clss]["attributes"].keys()

        tango_devices_cfg = get_class_devices(
            db, clss, clss_data[clss].get("filters", {})
        )

        # Foe each devices matches in the terms of each config entry (class, filters)
        # create attr_cfg_list and tango_db cache
        for device in tango_devices_cfg:

            # # Add a config enry to our dict
            attr_cfg_list[device] = {}
            tangoDB_polling_cache[device] = {}

            # Add YAML config parameters
            polling_setting = {
                attr_name.lower(): polling_setting
                for attr_name, polling_setting in clss_data[clss]["attributes"].items()
            }

            attr_cfg_list[device] = polling_setting

            # Init attribute default polling settings
            new_cfg_attrs = {attr.lower(): 0 for attr in yaml_attr_list}

            # Get device_name polling info from the TangoDB
            dev_polling_attr = db.get_device_property(device, "polled_attr")

            # if there is polling configuration
            # get all polled attributes and
            # add them in the cache (for later use)
            if (
                "polled_attr" in dev_polling_attr
                and len(dev_polling_attr["polled_attr"]) > 0
            ):
                # convert the polled_attr list to a dict
                polled_attrs = convert_poll_list(dev_polling_attr["polled_attr"])
                # attribute default polling settings
                # with the values from the TangoDB
                new_cfg_attrs.update(polled_attrs)

            # Update our Cache with the DB polling settings
            tangoDB_polling_cache[device] = new_cfg_attrs

    # Compare our yaml config with the tangodb cache
    # and append the differences in the diff list of tuples
    diff = compare_dicts(attr_cfg_list, tangoDB_polling_cache)
    # If we have differences
    if len(diff) == 0:
        # Exit now, no need to continue
        module.exit_json(**result)
    else:
        result["changed"] = True

    # if is not Check.mode we apply the changes to the TangoDB
    if not module.check_mode:

        # We use 2 methods to apply the polling based if
        # the TangoDS is Exposed or NOT.

        # For each entry in the Diff tuple (device,attr,old_value,new_value)

        for (
            device,
            attr,
            old_polling_value,
            new_polling_value,
        ) in diff:

            # for attr, polling_value in attr_data.items():

            # # Make a reference to our buffer and
            # # apply the changes there to be synced
            current_attr_props = tangoDB_polling_cache[device]
            current_attr_props.update({attr: new_polling_value})

            # We try to config polling via AttributeProxy
            # IF DEVICE IS EXPORTED
            # Otherwise we set the polling via DB object
            attr_proxy = tango.AttributeProxy("{0}/{1}".format(device, attr))

            try:
                # TODO is it faster to just
                # Check the old_polling_value == 0 ?
                if attr_proxy.is_polled():
                    attr_proxy.stop_poll()
                # set the polling
                attr_proxy.poll(new_polling_value)
                # If we manage to configure via attr proxy
                # dont have to continue to method config via DB
                continue
            except tango.DevFailed:
                module.log(msg="Device is not exported, trying via DB")

            # If we are here it means the Device is not exported
            # Therefore we are going to configure the polling via DB

            attr_polling_properties = []
            #
            for attr_db, polling_db in current_attr_props.items():
                attr_polling_properties.append(attr_db)
                attr_polling_properties.append(polling_db)

            # Lets put the property to the DB
            db.put_device_property(device, {"polled_attr": attr_polling_properties})

    # Generate Diff if requested
    if module._diff:

        diff_report = ["{0}/{1} {2} => {3}".format(*diff_input) for diff_input in diff]
        result.update({"diff": {"before": "", "after": "\n".join(diff_report)}})

    # # in the event of a successful module execution, you will want to
    # # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():

    run_module()


if __name__ == "__main__":
    main()
