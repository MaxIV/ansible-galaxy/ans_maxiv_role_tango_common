#!/usr/bin/python

# Copyright: (c) 2019, KITS <kits@maxiv.lu.se
# Written by Vasileios Martos <vasmar at maxiv.lu.se>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    "metadata_version": "1.1",
    "status": ["preview"],
    "supported_by": "kits_controls@maxiv.lu.se",
}

DOCUMENTATION = """
---
module: tango_attr

short_description: Tango Attribute read/write Module

version_added: "0.1"

description:
    - "Using Ansible in order to read/write idempotently a specified Tango attribute"

options:
    attr:
        description:
            - Attribute you want to read or write
        type: str
        required: true
    value:
        description:
            - Value you want to set
        type: str
        required: true
    force:
        description:
            - by default it is False, setting that option to True the module
              will force a write operation to the attribute
requirements:
- python-pytango >= 9.3.1
author:
    - KITS kits_controls@maxiv.lu.se
"""

EXAMPLES = """
  - name: Read Data from a Tango Attribute
    tango_attr:
      attr: "tango://tangodb:10000/test/dev/1/attribute"
    register: devicedata

  - name: Write a value to a Tango Attribute
    tango_attr:
      attr: "tango://tangodb:10000/test/dev/1/attribute"
      value: '15.0'

  - name: Force write a value to a Tango Attribute
    tango_attr:
      attr: "tango://tangodb:10000/test/dev/1/attribute"
      value: '15.0'
      force: true
"""

RETURN = """
has_failed:
    description: DeviceAttribute information if there is any failure in the attribute
    type: str
data_format:
    description: Data format of the attribute
    type: str
quality:
    description: The quality of the attribute value
    type: str
type:
    description: The type of the attribute
    type: str
value:
    description: The read value of the attribute
    type: str
w_value:
    description: The write value of the attribute
    type: str
"""


from ansible.module_utils.basic import AnsibleModule
from tango import AttributeProxy
import ast


def get_deviceattributedata(dev_attr):
    dev_dict = {
        "has_failed": str(dev_attr.has_failed),
        "data_format": str(dev_attr.data_format),
        "quality": str(dev_attr.quality),
        "type": str(dev_attr.type),
        "value": dev_attr.value,
        "w_value": dev_attr.w_value,
        "name": str(dev_attr.name),
    }
    return dev_dict


def compare_set_read_value(value1, value2):
    """
    # TODO not needed anymore since we are explicit in types
    # at the module parameters.
    Compares 2 values of the same type
    Returns True if values are the same
    else False
    """
    return ast.literal_eval(value1) == ast.literal_eval(value2)


# Module


def run_module():

    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        attr=dict(type="str", required=True),
        value=dict(type="str", required=False, default=None),
        force=dict(type="bool", required=False, default=False)
        # idempotence=dict(required=False, type='bool', default=False)
    )

    result = dict(changed=False)

    module = AnsibleModule(
        argument_spec=module_args,
        # required_if=required_if,
        supports_check_mode=True,
    )

    attr = module.params["attr"]
    value = module.params["value"]
    force = module.params["force"]

    # Create here the proxy and read the current DeviceAttribute
    attr_proxy = AttributeProxy(attr)
    try:
        before_device_attribute = attr_proxy.read()
    except Exception:
        module.fail_json(msg="Failed to read Attribute {0}".format(attr), **result)

    deviceattr_before_module = get_deviceattributedata(before_device_attribute)

    # If we only read attribute, just return the info and EXIT
    if not value:
        result["changed"] = False
        # Update result Return Values
        result.update(deviceattr_before_module)
        # annndd exit
        module.exit_json(**result)

    if module.check_mode:
        # Check mode and Force option are contradictive
        # We cannot force an operation which is in Check mode
        # Therefore we always Change == True
        if force:
            result["changed"] = True
        else:
            #     # if not force
            #     # Just Set the result depending on the value diff
            result["changed"] = not compare_set_read_value(
                deviceattr_before_module["value"], value
            )

    # Here we come for the normal write attribute function
    else:
        # If we force write always
        if force:
            try:
                attr_proxy.write(ast.literal_eval(value))
            except Exception:
                module.fail_json(
                    msg="Attribute {0} failed to write the value {1}".format(
                        attr, value
                    ),
                    **result
                )

            result["changed"] = True
        # if set values equal write dont change anything
        elif deviceattr_before_module["value"] == value:

            result["changed"] = False
        # if anything else (set_value =! value) and not force
        else:
            try:
                attr_proxy.write(ast.literal_eval(value))
            except Exception:
                module.fail_json(
                    msg="Attribute {0} failed to write the value {1}".format(
                        attr, value
                    ),
                    **result
                )
            result["changed"] = True

    after_device_attribute = attr_proxy.read()
    deviceattr_after_module = get_deviceattributedata(after_device_attribute)

    # Update result Return Values
    result.update(deviceattr_after_module)

    # # Print Diff
    if module._diff:
        result["diff"] = [{"before": deviceattr_before_module["value"], "after": value}]

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():

    run_module()


if __name__ == "__main__":
    main()
