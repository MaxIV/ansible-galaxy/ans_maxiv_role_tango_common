#!/usr/bin/python

# Copyright: (c) 2019, KITS <kits@maxiv.lu.se
# Written by Vasileios Martos <vasmar at maxiv.lu.se>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    "metadata_version": "1.1",
    "status": ["preview"],
    "supported_by": "kits_controls@maxiv.lu.se",
}

DOCUMENTATION = """
---
module: tango_cmd

short_description: Tango Attribute execute command

version_added: "0.1"

description:
    - "Using Ansible in order to execute a specified Tango command from a Device"

options:
    cmd:
        description:
            - command you want to execute
        type: str
        required: true
    param:
        description:
            - parameters of the command
        type: str
        required: false
requirements:
- python-pytango >= 9.3.1
author:
    - KITS kits_controls@maxiv.lu.se
"""

EXAMPLES = """
  - name: Execute Init Command in a Tango Device
    tango_cmd:
      cmd: "tango://tangodb:10000/test/dev/1/Init"
  tasks:
  - name: Execute a SetDouble in a Tango Device
    tango_cmd:
      cmd: "tango://tangodb:10000/test/dev/1/SetDouble"
      param: '15.0'
"""

RETURN = """
out:
    description: The return value of the command (If exists)
    type: str
"""


from ansible.module_utils.basic import AnsibleModule
from tango import DeviceProxy, DevFailed


def run_module():

    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        cmd=dict(type="str", required=True),
        param=dict(type="raw", required=False, default=None),
    )

    result = dict(changed=True, out=None)

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    cmd = module.params["cmd"]

    if module.params["param"]:
        param = module.params["param"]
    else:
        param = None

    # Prepare the DeviceProxy
    tango_list = cmd.split("/")
    tango_command = tango_list.pop()
    tango_device = "/".join(tango_list)

    try:
        tango_devproxy = DeviceProxy(tango_device)
    except DevFailed:
        module.fail_json(
            msg="Failed to create DevProxy for {0}".format(tango_device), **result
        )

    # if check mode we just do nothing and exit
    # otherwise we execute the command
    if not module.check_mode:

        try:
            return_value = tango_devproxy.command_inout(tango_command, param)
        except (DevFailed, TypeError):
            module.fail_json(
                msg="Failed to execute at Device: {0} Command: {1} "
                "with param: {2}".format(tango_device, tango_command, param),
                **result
            )

        result["out"] = return_value

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():

    run_module()


if __name__ == "__main__":
    main()
