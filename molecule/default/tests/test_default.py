import pytest


@pytest.fixture()
def distribution(host):
    return (host.system_info.distribution + str(host.system_info.release).split(".")[0]).lower()


@pytest.mark.parametrize("name", ["libtango9", "tango-common"])
def test_common_requirements(host, name):
    assert host.package(name).is_installed


def test_python2_pytango(host, distribution):
    if distribution != "rocky8":
        cmd = host.run('python -c "import tango; print(tango.Release.version)"')
        assert cmd.rc == 0
        assert cmd.stdout.strip().startswith("9")


def test_python3_pytango(host, distribution):
    pytango_versions = {
        "centos7": "python36-pytango",
        "rocky8": "python3-pytango"
    }
    if distribution != "centos6":
        pytango = pytango_versions[distribution]
        assert host.package(pytango).is_installed
        cmd = host.run('python3 -c "import tango; print(tango.Release.version)"')
        assert cmd.rc == 0
        assert cmd.stdout.strip().startswith("9")


def test_dsconfig(host, distribution):
    if distribution == "centos7":
        assert host.package("python-dsconfig").is_installed
        cmd = host.run('python -c "import dsconfig"')
        assert cmd.rc == 0
    if distribution == "rocky8":
        assert host.package("python3-dsconfig").is_installed
        cmd = host.run('python3 -c "import dsconfig"')
        assert cmd.rc == 0


def test_tango_host(host):
    hosts_with_tango_host = ["tango_common-tango-host-centos7", "tango_common-tango-host-rocky8"]
    if host.ansible.get_variables()["inventory_hostname"] in hosts_with_tango_host:
        tango_host = "mytango:12000"
    else:
        tango_host = "localhost:10000"
    assert host.file("/etc/tangorc").contains(f"^TANGO_HOST={tango_host}$")
    cmd = host.run(r'su -l -c "echo \$TANGO_HOST" root')
    assert cmd.stdout.strip() == tango_host


def test_default_java(host):
    cmd = host.run("java -version 2>&1")
    assert 'openjdk version "1.8.0' in cmd.stdout
