# ans_maxiv_role_tango_common

Ansible role to setup tango common requirements.

This role will configure `TANGO_HOST` in `/etc/tangorc` file.
You should set the `tango_host` variable in your inventory.

## Role Variables

See [defaults/main.yml](defaults/main.yml)

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ans_maxiv_role_tango_common
```

## License

GPL-3.0-or-later
